# -*- encoding: utf-8 -*-

import requests
from pprint import pprint
import sys

address = raw_input('Podaj adres: ')
#address = 'warszawa'
# Znalezienie współrzędnych adresu
google_address = 'https://maps.googleapis.com/maps/api/geocode/json'
google_params = {'address': address, 'sensor': 'false'}
google_json = requests.get(google_address, params=google_params).json()

if not google_json['results']:
    print 'Nie znaleziono'
    sys.exit()

lat = google_json['results'][0]['geometry']['location']['lat']
lng = google_json['results'][0]['geometry']['location']['lng']
location = '%s|%s' % (lat, lng)
# Znalezienie najbliższych geoskrytek
key = 'g93Ex2STFt2vNHqPav96'
op_adress = 'http://opencaching.pl/okapi/services/caches/search/nearest'
op_params = {'center': location, 'radius': 0.5, 'consumer_key': key}
op_json = requests.get(op_adress, params=op_params).json()

# Wydobycie informacji o lokalizacji skrytek
caches = '|'.join(op_json['results'])
base = 'http://opencaching.pl/okapi/services/caches/geocaches'
data = {'cache_codes': caches, 'consumer_key': key}
result = requests.get(base, params=data)
op2_json = result.json()
# Przydzielenie adresu skrytkom na podstawie ich współrzędnych
caches = []
for c in op2_json:
    address = op2_json[c]['location'].replace('|', ',')
    google_params = {'address': address, 'sensor': 'false'}
    google_json = requests.get(google_address, params=google_params).json()
    caches.append([op2_json[c]['status'], google_json['results'][0]['formatted_address']])
# Wyświetlenie wyniku
caches = sorted(caches, key=lambda a: a[0], reverse=False)

for c1 in caches:
    print '%s - %s' % (c1[0], c1[1])