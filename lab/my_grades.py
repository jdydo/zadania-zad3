# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup
import sys
import html5lib

url = {
    'login': 'https://canvas.instructure.com/login',
    'grades': 'https://canvas.instructure.com/courses/838324/grades'
}

#username = raw_input('Użytkownik: ')
#password = getpass.getpass('Hasło: ')

payload = {
    'pseudonym_session[unique_id]': '',
    'pseudonym_session[password]': ''
}


# Logowanie
session = requests.session()
session.post('https://canvas.instructure.com/login', data=payload)
# Pobranie strony z ocenami
response = session.get('https://canvas.instructure.com/courses/838324/grades')
# Odczyt strony
html = response.text
# Parsowanie strony
soup = BeautifulSoup(html, 'html5lib')
# Scraping
table = soup.find(id='grades_summary').find('tbody')
#print table
grades = []
grades.append([u'Aktywność', []])
grades.append([u'Prace domowe', []])
grades.append([u'Laboratoria', []])
grades.append([u'Egzamin', []])

for th in table.find_all('tr', 'student_assignment'):
    div = th.find('div', 'context')
    try:
        div = div.get_text()
        print th.find('a')['href'], th.find('span', 'score')
        if div == u'Aktywność':
            grades[0][1].append([th.find('a').get_text(), th.find('a')['href'], th.find('span', 'score').text])
        elif div == u'Prace domowe':
            grades[1][1].append([th.find('a').get_text(), th.find('a')['href'], th.find('span', 'score').text])
        elif div == u'Laboratoria':
            grades[2][1].append([th.find('a').get_text(), th.find('a')['href'], th.find('span', 'score').text])
        elif div == u'Egzamin':
            grades[3][1].append([th.find('a').get_text(), th.find('a')['href'], th.find('span', 'score').text])
    except:
        pass

# Sortowanie
for s in grades:
    s[1] = sorted(s[1], key=lambda s: s[2], reverse=True)
# Wyświetlenie posortowanych ocen w kategoriach
for f in grades:
    print f[0]
    for f1 in f[1]:
        print '\t%s - %s - %s' % (f1[0], f1[1], f1[2])
