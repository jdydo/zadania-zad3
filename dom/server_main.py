# -*- encoding: utf-8 -*-

from server_run_methods import *


# Metoda uruchamia serwer i obsługuje wyjątki serwerowe
def run_server(s):
    try:
        while True:
            connection, address = s.accept()
            add = str(address[0])
            try:
                request = connection.recv(2048)
                print request
                if request:
                    print "RECEIVED from %s" % add
                    connection.sendall(exam_request(request))
                    print "SEND to %s\r\n" % add
            except Exception, ex:
                try:
                    connection.sendall(create_500_error_content())
                    print "ERROR from: %s - %s\r\n" % (add, ex)
                except socket.error:
                    print "ERROR from: %s - %s\r\n" % (add, ex)
            finally:
                connection.close()
    except KeyboardInterrupt:
        s.close()


if __name__ == "__main__":
    run_server(create_socket())
