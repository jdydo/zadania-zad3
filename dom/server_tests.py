# -*- encoding: utf-8 -*-

import unittest
from client_get_resources_methods import get_games_json
from server_tests_methods import get_response, get_response_raw, check_content


### Testy wymagają włączonego serwera na komputerze zdalnym ###
class ServerTestCase(unittest.TestCase):

    #host = 'localhost'
    #port = 4444
    host = '194.29.175.240'
    port = 22334

    def test_200_(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/?steamid=76561197973215575')
        content = check_content(headers, 'application/json')
        self.assertEqual(status, 200)
        self.assertEqual(reason, 'OK')
        self.assertEqual(content, True)

    def test_422a(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/?steamid=7656119ddd3215575')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 422)
        self.assertEqual(reason, 'Unprocessable Entity')
        self.assertEqual(content, True)

    def test_422b(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/?steamid=76561197973')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 422)
        self.assertEqual(reason, 'Unprocessable Entity')
        self.assertEqual(content, True)

    def test_405(self):
        methods = ['POST', 'PUT', 'DELETE', 'HEAD']
        for method in methods:
            status, reason, headers = get_response(self.host, self.port, method)
            content = check_content(headers, 'text/html')
            self.assertEqual(status, 405)
            self.assertEqual(reason, 'Method Not Allowed')
            self.assertEqual(content, True)

    def test_404_directory(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 404)
        self.assertEqual(reason, 'Not Found')
        self.assertEqual(content, True)

    def test_404_content(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/obrazek.jpg')
        content = check_content(headers, 'text/html')
        self.assertEqual(status, 404)
        self.assertEqual(reason, 'Not Found')
        self.assertEqual(content, True)

    def test_headers(self):
        status, reason, headers = get_response(self.host, self.port, 'GET', '/obrazek.jpg')

        i = 0
        for header in headers:
            if 'date' in header[0] or 'content-type' in header[0] or 'content-length' in header[0] and header[1]:
                i += 1

        self.assertEqual(i, 3)
        self.assertEqual(len(headers), 3)

    def test_headers_raw_sip_protocol_request(self):
        request = 'INVITE sip:bob@biloxi.com SIP/2.0' + '\r\n' \
                    + 'Via: SIP/2.0/UDP pc33.atlanta.com;branch=z9hG4bK776asdhds' + '\r\n' \
                    + 'Max-Forwards: 70' + '\r\n' \
                    + 'To: Bob <sip:bob@biloxi.com>' + '\r\n' \
                    + 'From: Alice <sip:alice@atlanta.com>;tag=1928301774' + '\r\n' \
                    + 'Call-ID: a84b4c76e66710@pc33.atlanta.com' + '\r\n' \
                    + 'CSeq: 314159 INVITE' + '\r\n' \
                    + 'Contact: <sip:alice@pc33.atlanta.com>' + '\r\n' \
                    + 'Content-Type: application/sdp' + '\r\n' \
                    + 'Content-Length: 142' + '\r\n\r\n'
        response = get_response_raw(self.host, self.port, request)
        main_header = response.split('\r\n')[0]
        self.assertEqual(main_header, 'HTTP/1.1 405 Method Not Allowed')

    def test_headers_raw_odd_request(self):
        client_headers = 'ążśęćó łńasd_ 123[]];.../ w \r\nążśęćó łńasd_ 123[]];.../ w \r\nążśęćó3[]];.../ w \r\n\r\n'
        server_headers = get_response_raw(self.host, self.port, client_headers)
        main_header = server_headers.split('\r\n')[0]
        self.assertEqual(main_header, 'HTTP/1.1 405 Method Not Allowed')

    def test_json(self):
        json_game = get_games_json(76561197973215575)
        self.assertGreater(len(json_game), 0)
        self.assertIsNotNone(json_game)
        self.assertTrue('games' in json_game.keys())
        self.assertIsNotNone(json_game['games'][0]['title'])
        self.assertIsNotNone(json_game['games'][0]['appid'])
        self.assertIsNotNone(json_game['games'][0]['allegro'])
        self.assertIsNotNone(json_game['games'][0]['steam_price'])
        self.assertIsNotNone(json_game['games'][0]['videos']['review'])
        self.assertIsNotNone(json_game['games'][0]['videos']['gameplay'])
        self.assertIsNotNone(json_game['games'][0]['videos']['trailer'])

    def test_json_error1(self):
        self.assertRaises(ValueError, lambda: get_games_json(7656119797321557))

    def test_json_error2(self):
        self.assertRaises(ValueError, lambda: get_games_json('76561dddd1557'))

    def test_json_error3(self):
        self.assertRaises(ValueError, lambda: get_games_json(''))



if __name__ == '__main__':
    unittest.main()
