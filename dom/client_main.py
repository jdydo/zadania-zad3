# -*- encoding: utf-8 -*-

import requests
from client_html_methods import get_full_html, open_web_browser
from client_get_resources_methods import get_steamid, get_games_json


# Metoda łączy z serwerem zdalnym, wysyła i odbiera dane
def connect():
    try:
        vanity_url = raw_input(u"Podaj link do profilu, który chcesz sprawdzić.\n"
                               u"Przykład: http://steamcommunity.com/id/Kira-qun\n>>> ")
        #vanity_url = "http://steamcommunity.com/id/Kira-qun"
        #vanity_url = "http://steamcommunity.com/id/SR_SEDES"

        steamid = get_steamid(vanity_url)
        print u'Znaleziono użytkownika: %s' % steamid

        print u'Wysłano zapytanie. Trwa pobieranie informacji. Proszę czekać...'
        games = get_games_json(steamid)
        print u'Otrzymano wszystkie dane. Generowanie strony html...'
        get_full_html(vanity_url, steamid, games)
        open_web_browser()
    except requests.Timeout:
        print u'BŁĄD! Minął czas połączenia! Możliwe, że api jest w tej chwili niedostępne. Proszę spróbować później.'
    except ValueError:
        print u'BŁĄD! Wprowadzono nieporawne dane, wybrany użytkownik nie grał ostatnio w żadną grę ' \
              u'lub API jest w tej chwili niedostępne!'
    except Exception:
        print u'BŁĄD! Brak połączenia.'
    finally:
        print u'Program zakończył działanie.'


if __name__ == "__main__":
    connect()