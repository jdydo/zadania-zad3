# -*- encoding: utf-8 -*-

import requests
import sys
from xml.etree import ElementTree


# Metoda łączy się i pobiera informacje z serwera zdalnego
def get_games_json(steam_id):
    #api_address = "http://localhost:4444"
    api_address = "http://194.29.175.240:22334"
    api_params = {"steamid": steam_id}
    api_json = requests.get(api_address, params=api_params, timeout=120.0).json()

    return api_json


# Metoda pobiera steamID64 na podstawie linku profilowego
def get_steamid(vanity_url):
    try:
        api_params = {"xml": 1}
        api_xml = requests.get(vanity_url, params=api_params, timeout=10.0)
        xml_tree = ElementTree.fromstring(api_xml.content)
        id = xml_tree.find('steamID64').text

        if not id.isdigit() and not len(id) == 17:
            raise

        return id

    except Exception, ex:
        print u'BŁĄD! Podałeś zły link lub usługa Steam Community jest niedstępna. Sprobuj ponownie później!'
        sys.exit()

