# -*- encoding: utf-8 -*-

import sys
import os
import webbrowser


# Metoda tworzy gotowy segment html dla pojedynczej gry
def get_game_html(game_title, appid, game_price, allegro_href, review_id, gameplay_id, trailer_id):
    html = '''
        <div style="margin-top: 30px; margin-bottom: 30px ;margin-left: auto; margin-right: auto; width: 1000px;">
            <h1>%s</h1>

            <div>
                <a href="http://store.steampowered.com/app/%s">STEAM - cena: %s &euro;</a>
            </div>
            <div style="; margin:  20px; margin-top: 30px">
                <div style="margin-bottom: 10px">Sprawdz oferty na allegro:</div>
                <a href="%s"><img src="css/all.jpg"alt="allegro_link"/></a>
            </div>
            <div style="margin-left: auto; margin-right: auto; width: 1000px; height: 220px">
                <div class="vid">
                    <div style="margin: 10px">
                        Recenzja
                    </div>
                    <iframe title="YouTube video player" class="youtube-player" type="text/html" width="312"
                            height="176" src="http://www.youtube.com/embed/%s" frameborder="0"
                            allowFullScreen></iframe>
                </div>

                <div class="vid">
                    <div style="margin: 10px">
                        Gameplay
                    </div>
                    <iframe title="YouTube video player" class="youtube-player" type="text/html" width="312"
                            height="176" src="http://www.youtube.com/embed/%s" frameborder="0"
                            allowFullScreen></iframe>
                </div>

                <div class="vid">
                    <div style="margin: 10px">
                        Trailer
                    </div>
                    <iframe title="YouTube video player" class="youtube-player" type="text/html" width="312"
                            height="176" src="http://www.youtube.com/embed/%s" frameborder="0"
                            allowFullScreen></iframe>
                </div>
            </div>
        </div>
        <hr />
        '''
    return html % (game_title, appid, game_price, allegro_href, review_id, gameplay_id, trailer_id)


#Metoda łączy gotowe segmenty html ze sobą
def get_full_game_list_html(games):
    game_list = ''
    for game in games['games']:
        game_list += get_game_html(game['title'], game['appid'], game['steam_price'], game['allegro'],
                                   game['videos']['review'], game['videos']['gameplay'], game['videos']['trailer'])
    return game_list


#Metoda tworzy gotową stronę html przedstawiającą pobrane informacje
def get_full_html(vanity_url, steamid, games):
    template = open('template/web.htm').read()
    new_page = open('data/result.htm', 'w')
    url = vanity_url.split('/')
    nick = url[len(url) - 1]
    new_page.write(template % (nick, steamid, get_full_game_list_html(games).encode('ascii', 'ignore')))
    new_page.close()


# Metoda otwiera stronę wynikową w przeglądarce
def open_web_browser():
    url = os.path.normcase('file://%s/data/result.htm' % os.path.dirname(os.path.realpath(__file__)))
    webbrowser.open(url)
    sys.exit()