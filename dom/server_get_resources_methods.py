# -*- encoding: utf-8 -*-

import requests
from urllib import quote_plus


# Metoda pobiera identyfikatory filmów dla odpowiedniej gry
def get_youtube_videos(games):
    video_types = ["review", "trailer", "gameplay"]
    youtube_address = "https://www.googleapis.com/youtube/v3/search?"
    for game in games["games"]:
        videos = {}
        for video_type in video_types:
            keywords = ("%s %s" % (game["title"], video_type)).replace(" ", "+")
            youtube_params = \
                {
                    "part": "snippet",
                    "maxResults": 1,
                    "order": "relevance",
                    "q": keywords,
                    "type": "video",
                    "videoDefinition": "any",
                    "videoEmbeddable": "true",
                    "key": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
                }
            try:
                youtube_json = requests.get(youtube_address, params=youtube_params, timeout=30.0).json()
                video_id = youtube_json["items"][0]["id"]["videoId"]
            except Exception, ex:
                video_id = ""
            videos[video_type] = video_id
        game["videos"] = videos
    return games


# Metoda tworzy linki allegro dla każdej przesłanej gry
def get_allegro_href(games):
    allegro_address = "http://allegro.pl/komputerowe-pc-45713?order=p&string=%s"
    for game in games["games"]:
        game["allegro"] = allegro_address % quote_plus(game["title"])
    return games


# Metoda pobiera ostatnio grane gry
def get_recently_played_steam_games(steam_id):
    steam_address = "http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/"
    steam_params = {"key": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", "steamid": steam_id, "format": "json"}
    steam_json = requests.get(steam_address, params=steam_params, timeout=20.0).json()

    games = {"games": []}
    for game in steam_json["response"]["games"]:
        games["games"].append({"title": game["name"].encode('ascii', 'ignore'), "appid": game["appid"]})

    return games


# Metoda pobiera ceny danych gier na Steam
def get_steam_price(games):
    steam_store_address = "http://store.steampowered.com/api/appdetails"
    for game in games["games"]:
        steam_store_params = {"appids": game["appid"]}
        try:
            steam_store_json = requests.get(steam_store_address, params=steam_store_params, timeout=10.0).json()
            game["steam_price"] = float(steam_store_json[str(game["appid"])]["data"]["price_overview"]["final"])/100.0
        except Exception, ex:
            game["steam_price"] = 0.0
    return games
