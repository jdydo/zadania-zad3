# -*- encoding: utf-8 -*-

import socket
import json
import mimetypes
from urlparse import urlparse, parse_qs
from email.utils import formatdate
from server_get_resources_methods import *


# Metoda tworzy gotowy do użycia socket
def create_socket():
    #host = 'localhost'
    #port = 4444
    host = '194.29.175.240'
    port = 22334

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_IP)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host, port))
    s.listen(1)

    print "Serwer ONLINE\r\n"
    return s


# Metoda tworzy treść strony w zależności od żądania
def exam_request(request):
    status_code, parameter = check_request(request)

    if status_code == 200:
        status = 'HTTP/1.1 200 OK'
        content, page = return_json(parameter)
    elif status_code == 404:
        status = 'HTTP/1.1 404 Not Found'
        content = mimetypes.guess_type('error_pages/404.html')[0]
        page = open('error_pages/404.html').read()
    elif status_code == 422:
        status = 'HTTP/1.1 422 Unprocessable Entity'
        content = mimetypes.guess_type('error_pages/422.html')[0]
        page = open('error_pages/422.html').read()
    else:
        status = 'HTTP/1.1 405 Method Not Allowed'
        content = mimetypes.guess_type('error_pages/405.html')[0]
        page = open('error_pages/405.html').read()

    return create_header(status, content, page) + page


# Metoda tworzy nagłówek dla błędu 500 oraz wczytuje odpowiednią stronę z błędem
def create_500_error_content():
    status = 'HTTP/1.1 500 Internal Server Error'
    content = mimetypes.guess_type('error_pages/500.html')[0]
    page = open('error_pages/500.html').read()

    return create_header(status, content, page) + page


# Metoda konstruuje nagłówek na podstawie przesłanyh argumentów
def create_header(status, content, page):
    header = status + '\r\n' \
             + 'Content-Type: ' + content + '; charset=UTF-8' + '\r\n' \
             + 'Content-Length: ' + str(len(page)) + '\r\n' \
             + 'Date: ' + formatdate(timeval=None, localtime=False, usegmt=True) + '\r\n\r\n'

    return header


# Metoda uruchamia kolejno funkcje pobierajace i przetwarzające dane o grach
def return_json(parameter):
    games = get_recently_played_steam_games(parameter['steamid'][0])
    print "PROCESSED - get_recently_played_steam_games(request)"
    get_youtube_videos(games)
    print "PROCESSED - get_youtube_videos(games)"
    get_allegro_href(games)
    print "PROCESSED - get_allegro_href(games)"
    get_steam_price(games)
    print "PROCESSED - get_steam_price(games)"
    return 'application/json', json.dumps(games)


# Metoda sprawdza czy żądanie jest prawidłowe
def check_request(request):
    main_header = request.split('\r\n')[0]
    main_header_elements = main_header.split(' ')
    if ('GET' in main_header) and ('HTTP' in main_header) and (len(main_header_elements) == 3):
        parameters = parse_qs(urlparse(request.split(' ')[1]).query)
        print parameters
        if (len(parameters) == 1) \
                and ('steamid' in parameters.keys()) \
                and parameters['steamid'][0].isdigit() \
                and len(parameters['steamid'][0]) == 17:
            return 200, parameters
        elif (len(parameters) == 1) \
                and ('steamid' in parameters.keys()) \
                and (not parameters['steamid'][0].isdigit() or not (len(parameters['steamid'][0]) == 17)):
            return 422, None
        else:
            return 404, None
    else:
        return 405, None
