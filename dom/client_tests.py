# -*- encoding: utf-8 -*-

import unittest
from client_get_resources_methods import get_steamid
from client_html_methods import get_full_html


### Testy nie wymagają włączonego serwera na komputerze zdalnym ###
class ClientTestCase(unittest.TestCase):
    def test_get_steamid(self):
        steamid = get_steamid('http://steamcommunity.com/id/Kira-qun')
        self.assertEqual('76561197973215575', steamid)

    def test_get_steamid_error1(self):
        self.assertRaises(SystemExit, lambda: get_steamid('http://stesąśćżźfdsffsdKira-qun'))

    def test_get_steamid_error2(self):
        self.assertRaises(SystemExit, lambda: get_steamid(''))

    def test_html_filling(self):
        games = {"games": [{"appid": 102500,
                            "allegro": "http://allegro.pl/komputerowe-pc-45713?order=p&string=Kingdoms+of+Amalur%3A+Reckoning",
                            "steam_price": 19.99,
                            "videos": {"review": "jVC6FemwPcI",
                                       "gameplay": "ZBo3W318aNE",
                                       "trailer": "9KI9E3T33lM"},
                            "title": "Kingdoms of Amalur: Reckoning"}]}
        vanity_url = 'http://steamcommunity.com/id/Kira-qun'
        steamid = '76561197973215575'
        html1 = open('data/result.htm').read()
        html2 = open('template/web.htm').read()
        tab1 = ['102500', 'http://allegro.pl/komputerowe-pc-45713?order=p&string=Kingdoms+of+Amalur%3A+Reckoning',
                '19.99', 'jVC6FemwPcI', 'ZBo3W318aNE', '9KI9E3T33lM', 'Kingdoms of Amalur: Reckoning',
                'Kira-qun', '76561197973215575']

        get_full_html(vanity_url, steamid, games)

        for t in tab1:
            self.assertTrue(t in html1)
            self.assertFalse(t in html2)

        self.assertFalse('%s' in html1)
        self.assertTrue('%s' in html2)

if __name__ == '__main__':
    unittest.main()
